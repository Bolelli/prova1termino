import java.util.ArrayList;
public class Disciplina{

    private String nomeDisciplina;
    private String codigo;
    private ArrayList<Aluno> listaAlunoDaDisciplina;

    //construtor
    public Disciplina(){
    listaAlunoDaDisciplina = new ArrayList<Aluno>();
    }

    //metodos
   
    
    public void setNomeDisciplina(String umNomeDisciplina){
      nomeDisciplina = umNomeDisciplina; 
    }

     public String getNomeDisciplina(){
    	return nomeDisciplina;
     }

     public void setCodigo(String umCodigo){
      codigo = umCodigo; 
    }

     public String getCodigo(){
		return codigo;
     }



}