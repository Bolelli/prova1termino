public class Aluno {

    //Atributos

    private String nome;
    private String matricula;

    //Construtor

    public Aluno(){
		
	}

	public Aluno(String nome, String matricula){
		nome = nome;
		matricula = matricula;
	}

	//Metodos

	public void setNome(String umNome){
		nome = umNome;
	}
	
    public String getNome(){
		return nome;
        }
        
        public void setMatricula(String umaMatricula){
		matricula = umaMatricula;
	}
	public String getMatricula(){
		return matricula;
	}

}
