import java.util.ArrayList;
 
 public class ControleAluno{

    //Atributos
    private ArrayList<Aluno> listaAluno;

	//Construtor
	public	ControleAluno(){
		listaAluno = new ArrayList<Aluno>();
	}

	//Metodos

	public String adicionar(Aluno umAluno){
		String mensagem = "Aluno adicionado com sucesso";
		listaAluno.add(umAluno);
		return mensagem;
	}
	
    public Aluno buscar(String nome){
    for(Aluno aluno: listaAluno){
	    if(aluno.getNome().equalsIgnoreCase(nome)) return aluno ;
	}
   return null;
	}


public Aluno buscarRemover(String nome){
	for(Aluno aluno: listaAluno){
	    if(aluno.getNome().equalsIgnoreCase(nome)) return aluno ;
            listaAluno.remove(aluno);
	}
   return null;
	}
}